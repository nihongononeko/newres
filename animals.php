<!DOCTYPE html>

<html lang="кг">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>

<?php

    $zoo = [
        "Africa" => ["Crocodylus acutus", "Hippopotamus amphibius", "Giraffa camelopardalis", "Diceros bicornis", "Loxodonta africana"],
        "Australia" => ["Ornithorhynchus anatinus", "Macropus", "Vombatus ursinus", "Phascolarctos cinereus", "Thylacinus cynocephalus"],
        "Eurasia" => ["Dipodidae", "Canis lupus", "Ursus thibetanus", "Meles meles", "Alces alces"],
        "Antarctica" => ["Belgica antarctica", "Euphausia superba", "Macronectes richmond", "Aptenodytes forsteri", "Cryolophosaurus"],
    ];

    $zoo1 = []; // массив с первыми словами из названий животных
    $zoo2 = []; // массив со вторыми словами из названий животных

    foreach ($zoo as $area => $animals) {
        foreach ($animals as $animal_name) {

            if (substr_count($animal_name, " ") == 1) {

                $twoWords = explode(" ", $animal_name);

                array_push($zoo1, $twoWords[0]);

                array_push($zoo2, $twoWords[1]);

            }
        }
    }

    shuffle($zoo1); // перемешиваем $zoo_4
    shuffle($zoo2); // перемешиваем $zoo_5


    for ($i = 0; $i < count($zoo1); $i++) {

        echo $zoo1[$i]." ";
        echo $zoo2[$i]."<br>";

    }

    $zoo3 = $zoo;

    foreach ($zoo3 as $area => $animals) {
        
        foreach ($animals as $k => $animal_name) {
            if (substr_count($animal_name, " ") == 1) {
                $zoo3[$area][$k] = strstr($zoo3[$area][$k], " ", true);
            } else {
                unset($zoo3[$area][$k]);
            }
        }

    }

    foreach ($zoo3 as $area => $animals) {
        foreach ($animals as $k => $animal_name) {
            $rand = array_rand($zoo2, 1);
            $zoo3[$area][$k] = $zoo3[$area][$k]." ".$zoo2[$rand];
            unset($zoo2[$rand]);
        }

    }

    foreach ($zoo3 as $area => $animals) {
        $animals = array_values($animals);
        echo "<h2> $area </h2>";
        echo "<p>";
        for ($i = 0; $i < count($animals); $i++) {
            echo $animals[$i];
            // echo $i;
            if ($i == (count($animals)-1)) {
                echo ".";
            } else {
                echo ", ";
            }
        }
        echo "</p>";
    }

?>

</body>
</html>