<?php

$name = !empty($_POST['name']) ? ($_POST['name']) : '';

$author = !empty($_POST['author']) ? ($_POST['author']) : '';

$isbn = !empty($_POST['isbn']) ? ($_POST['isbn']) : '';

// htmlspecialchars

$pdo = new PDO('mysql:host=localhost;dbname=normalnaya_bd;charset=utf8', 'root', '');

$inquiry = 'SELECT name, author, year, isbn, genre FROM books WHERE name LIKE ? AND author LIKE ? AND isbn LIKE ?';

$sth=$pdo->prepare($inquiry);

$sth->execute(["%$name%", "%$author%", "%$isbn%"]);

$result=$sth->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <title>start</title>
</head>
<body>

<form method="post" >
    <input type="text" placeholder="название книги" name="name" value="<?php echo htmlspecialchars($name); ?>">
    <input type="text" placeholder="Автор" name="author" value="<?php echo htmlspecialchars($author); ?>">
    <input type="text" placeholder="ISBN" name="isbn" value="<?php echo htmlspecialchars($isbn); ?>">
    <input type="submit" value="Найти">
</form>

<table>
    <tr>
        <td>Название книги</td>
        <td>Автор</td>
        <td>Год издания</td>
        <td>ISBN</td>
        <td>Жанр</td>
    </tr>

    <?php foreach ($result as $arr) : ?>

        <tr>

        <?php foreach ($arr as $val) : ?>

            <td> <?php echo $val; ?> </td>

        <?php endforeach ?>

        </tr>

    <?php endforeach ?>

</table>

</body>
</html>